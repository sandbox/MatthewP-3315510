<?php

namespace Drupal\cloudflare_country_block\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
define('CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER', 'HTTP_CF_IPCOUNTRY');
class cloudflare_country_blockSubscriber implements EventSubscriberInterface {

  //check cloudflare ip then block if necessary

  public function checkForRedirection(RequestEvent $event) {

    $config = \Drupal::config('cloudflare_country_block.settings');

        $account = \Drupal::currentUser();

		 $countryblockenabled = $config->get('cloudflare_country_block_enabled');

         //check if admin or on drush
	  if ((PHP_SAPI === 'cli') || ($account->id() == 1)) {
		return;
	  }


		  // Firstly check if the blocking mechanism is enabled at all.
  if ($countryblockenabled) {
    $block_access = FALSE;
    $country_code = '';

    // Get country code from Cloudflare request headers.
    if (isset($_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER])) {
      $country_code = $_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER];

      $blocked_countries = $config->get('cloudflare_country_block_countries', array());

      if (array_key_exists($country_code, $blocked_countries)) {
        // This is a match! We're being visited from a country that was selected
        // to be blocked!
        $block_access = TRUE;

      }
    }
    else {
      // We did not get the Cloudflare request header, so the config decides.
      if ($config->get('cloudflare_country_block_cloudflare_missing', 'nothing') == 'block') {
        $block_access = TRUE;
      }
    }
     $clientip = $_SERVER["HTTP_CF_CONNECTING_IP"];
    // Check if the access has to be blocked.

    if ($block_access) {
      // Log blocking if configured.
      if ($config->get('cloudflare_country_block_watchdog') == TRUE) {
        if ($country_code == '') {
         \Drupal::logger('cloudflare_country_block')->notice( 'Blocked request from IP :ip with an empty country code.', array(':ip' => $clientip));
        }
        else {
         \Drupal::logger('cloudflare_country_block')->notice('Blocked request from IP :ip with country code :country' , array(
            ':ip' => $clientip,
            ':country' => $country_code,
          ));
        }
      }

      // Pet header code according to config.
      $response_code = $config->get('cloudflare_country_block_response_code');
      //module_load_include('inc', 'cloudflare_country_block', 'http_response_codes');
	  \Drupal::service('module_handler')->loadInclude('cloudflare_country_block', 'inc', 'http_response_codes');
      $response_codes = cloudflare_country_block_get_http_codes();
      header('HTTP/1.0 ' . $response_codes[$response_code]['technical_response']);

      // Performing exit according to chosen method.
      switch ($config->get('cloudflare_country_block_exit_method', 'drupal_exit')) {
        case 'php_exit':
          exit;

        case 'access_denied':
          throw new NotFoundHttpException();
          break;

        case 'drupal_exit':
        default:
           throw new ServiceUnavailableHttpException(3, t('Error'));
          // Just to be nice code and prevent copy paste errors.
          break;

      }

    }
  }


	/*if ($event->getRequest()->query->get('redirect-me')) {
      $event->setResponse(new RedirectResponse('http://example.com/'));
    }
  }*/

}
  /**
   * {@inheritdoc}
   */

  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForRedirection');
    return $events;
  }


}
