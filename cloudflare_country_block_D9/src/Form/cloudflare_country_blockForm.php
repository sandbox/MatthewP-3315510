<?php
// Matthew Pellegrino cloudflare_country_block drupal 9.x module
/**
* @file
* Contains \Drupal\cloudflare_country_block\Form\cloudflare_country_blockForm
*/

namespace Drupal\cloudflare_country_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

class cloudflare_country_blockForm extends ConfigFormBase {



/**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'cloudflare_country_block.settings';

   /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

 /**
 * {@inheridoc}
 */
  public function getFormId(){

    return 'cloudflare_country_blockForm';
  }

  /**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    // Get the config object.
    $config = $this->config('cloudflare_country_block.settings');

    $storage = $form_state->getStorage();
    //$config->set('cloudflare_country_block_enabled', TRUE);
    // Save all the items from $form_state.
    foreach ($form_state->getValues() as $key => $value) {

        $config->set($key, $value);

    }

    // Save the honeypot forms from $form_state into a 'form_settings' array.
    $config->set('form_settings', $form_state->getValue('form_settings'));

    $config->save();

    // Clear the honeypot protected forms cache.
    //$this->cache->delete('honeypot_protected_forms');

    parent::submitForm($form, $form_state);



  }

  /**
  * {@inheritdoc}
  */
	public function buildForm(array $form, FormStateInterface $form_state)
	{

		$config = $this->config('cloudflare_country_block.settings');


		 $form['cloudflare_country_block'] = array(
						'#type' => 'fieldset',
						'#title' => $this->t('cloudflare country block'),
						'#description' => $this->t('Block users from other countries'),
						'#collapsible' => FALSE,
					);



			/*
			  We have the following configuration options:
			  - Is the blocker enabled or disabled?
			  - Which countries are being blocked?
			  - Which HTTP return code is being given when a country is matching?
			  - Do or do not log this to Watchdog
			  - Which exit method is preferred?
			  - How should the module react if it is enabled and the Cloudflare response
			  header is not given?
			*/

			  // Get the actual cloudflare request value from the request parameters.
			  $cloudflare_value = isset($_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER]) ? $_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER] : FALSE;

			  $form['cloudflare_country_block_information_title'] = array(
				'#type' => 'markup',
				'#markup' => t('Cloudflare Request Information'),
				'#prefix' => '<h2>',
				'#suffix' => '</h2>',
				'#weight' => 1,
			  );

			  $form['cloudflare_country_block_information'] = array(
				'#type' => 'markup',
				'#markup' => ($cloudflare_value === FALSE ?
				  t('<p style="color:#CD5C5C">The Cloudflare identifier was not found in the request! This means you did not reach this page via Cloudflare.</p>') :
				  t('<p style="color:#90EE90">The Cloudflare identifier was found in the request: :identifier</p>', array(':identifier' => $cloudflare_value))
				),
				'#suffix' => '<hr><br>',
				'#weight' => 2,
				'#cache' => array(
				  'max-age' => 0,
				),
			  );

			  $form['cloudflare_country_block_enabled'] = array(
				'#type' => 'checkbox',
				'#title' => t('<strong>Enable Blocking</strong>'),
				'#default_value' => $config->get('cloudflare_country_block_enabled'),
				'#description' => t('Enable the blocking mechanism using the criteria below.'),
				'#weight' => 3,
			  );

			  //module_load_include('inc', 'cloudflare_country_block', 'country_codes');
			  \Drupal::service('module_handler')->loadInclude('cloudflare_country_block', 'inc', 'country_codes');

			  $form['cloudflare_country_block_countries'] = array(
				'#type' => 'select',
				'#title' => t('Blocked countries'),
				'#default_value' => $config->get('cloudflare_country_block_countries'),
				'#options' => cloudflare_country_block_get_countries(),
				'#multiple' => TRUE,
				'#description' => t('Select the countries that should be blocked from accessing your site.<br>Select multiple by pressing Ctrl + Click.'),
				'#weight' => 4,
			  );

			  //module_load_include('inc', 'cloudflare_country_block', 'http_response_codes');
			  \Drupal::service('module_handler')->loadInclude('cloudflare_country_block', 'inc', 'http_response_codes');

			  $http_code_options = array();
			  foreach (cloudflare_country_block_get_http_codes() as $code => $info) {
				$http_code_options[$code] = $info['name'];
			  }

			  $form['cloudflare_country_block_response_code'] = array(
				'#type' => 'select',
				'#title' => t('HTTP Response Code'),
				'#default_value' => $config->get('cloudflare_country_block_response_code'),
				'#options' => $http_code_options,
				'#description' => t("Choose the HTTP response code that should be returned if the visitor's access is being blocked.<br>This is ignored if the access denied page is being shown as exit method below."),
				'#weight' => 5,
				'#required' => TRUE,
			  );

			  $form['cloudflare_country_block_watchdog'] = array(
				'#type' => 'checkbox',
				'#title' => t('Log Info to watchdog'),
				'#default_value' => $config->get('cloudflare_country_block_watchdog'),
				'#description' => t('If enabled, a watchdog info log message is written in case of a blocked access.'),
				'#weight' => 6,
			  );

			  $form['cloudflare_country_block_exit_method'] = array(
				'#type' => 'select',
				'#title' => t('Drupal Exit Method'),
				'#default_value' => $config->get('cloudflare_country_block_exit_method'),
				'#options' => array(
				  'drupal_exit' => t('Exit using a clean Drupal Shutdown'),
				  'php_exit' => t('Exit using an immediate PHP exit'),
				  'access_denied' => t('Display the Drupal access denied page'),
				),
				'#description' => t('Choose on how your site will react to an access to be blocked.<br>Note: The access denied page does not really reduce page load from these request, because this is a fully rendered page.'),
				'#weight' => 7,
				'#required' => TRUE,
			  );

			  $form['cloudflare_country_block_cloudflare_missing'] = array(
				'#type' => 'select',
				'#title' => t('What if Cloudflare information is not present?'),
				'#default_value' => $config->get('cloudflare_country_block_cloudflare_missing', 'nothing'),
				'#options' => array(
				  'nothing' => t('Do not validate and let them through'),
				  'block' => t('Block access as if it were a match'),
				),
				'#description' => t('When somehow a direct access is made on your server, the Cloudflare request information is not given. You may choose here on how to react in that situation.<br><strong>This is only considered if the Blocking mechanism is enabled.</strong>'),
				'#weight' => 8,
				'#required' => TRUE,
			  );




		return parent::buildForm($form, $form_state);

	}
	 protected function getFormSettingsValue(array $form_settings, string $form_id) {
    // If there are settings in the array and the form ID already has a setting,
    // return the saved setting for the form ID.
    if (!empty($form_settings) && isset($form_settings[$form_id])) {
      return $form_settings[$form_id];
    }
    // Default to false.
    else {
      return FALSE;
    }
  }
}
?>
