<?php

/**
 * Return all HTTP status codes that can make sense to return.
 * These are made available to choose on the configuration form.
 *
 * @return array
 */
function cloudflare_country_block_get_http_codes() {
  return array(
    '200' => array(
      'name' => t('200 OK'),
      'technical_response' => '200 OK',
    ),
    '400' => array(
      'name' => t('400 Bad Request'),
      'technical_response' => '400 Bad Request',
    ),
    '401' => array(
      'name' => t('401 Unauthorized'),
      'technical_response' => '401 Unauthorized',
    ),
    '403' => array(
      'name' => t('403 Forbidden'),
      'technical_response' => '403 Forbidden',
    ),
    '404' => array(
      'name' => t('404 Not found'),
      'technical_response' => '404 Not found',
    ),
    '410' => array(
      'name' => t('410 Gone'),
      'technical_response' => '410 Gone',
    ),
    '418' => array(
      'name' => t('418 I\'m a teapot'),
      'technical_response' => '418 I\'m a teapot',
    ),
    '429' => array(
      'name' => t('429 Too Many Requests'),
      'technical_response' => '429 Too Many Requests',
    ),
    '500' => array(
      'name' => t('500 Internal Server Error'),
      'technical_response' => '500 Internal Server Error',
    ),
    '503' => array(
      'name' => t('503 Service Unavailable'),
      'technical_response' => '503 Service Unavailable',
    ),
  );
}
