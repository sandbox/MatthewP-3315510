

This module will help you block users from specific countries from accessing the site.

- This is a drupal 9/10 version of the module located at this project page:
https://www.drupal.org/project/cloudflare_country_block


CONFIGURATION
-------------

   - Visit /admin/config/system/cloudflare-country-block

   - Check that the Cloudflare Request Information is given first!

   - Enable the blocking mechanism

   - Select which countries should be banned from your site.

   - Select response codes, logging details and exit behavior.

   - Choose the behavior when the Cloudflare request information is not given (e.g. on an
     unauthorized direct access). CAUTION: If you do not have the information in the request
     and choose to block accesses that do not provide this, you may lock yourself out!).
     The user 1 (super administrator) can not be locked out.


MAINTAINERS
-----------

Current maintainers:

 * Matthew Pellegrino - www.drupal.org/u/matthew-p
 * Florian Müller (fmueller_previon) - https://www.drupal.org/user/3524567
